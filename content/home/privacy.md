---
title: "Privacy and Security"
weight: 5
---

While you can load images already stored on your phone, the app does
not store images or data between sessions. No personal data is
collected or sent to any server after you install the app. There are
neither third party server integrations, accounts, analytics,
advertising, nor hidden trackers in this app.

The app has been used with sensitive and restricted materials because
it does not retain the temporarily captured image.
