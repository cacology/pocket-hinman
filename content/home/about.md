---
title: "About"
weight: 1
---

PocketHinman runs on iOS or Android and compares an image with what the camera sees.  We designed it to be a handheld [Hinman Collator](https://www.youtube.com/watch?v=AHzn5y4KcJk), but it works for anything.

## Physical texts

![Babbitt](Babbitt-real.gif)  

## Digital to physical texts

![Babbitt](Babbitt.gif)

## Objects

![shelf](shelf.gif)  

## Textile warp spacing

![warp spacing](warp%20spacing.gif)
