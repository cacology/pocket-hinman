---
title: "Design Philosophy"
weight: 4
---

Communities build tools to solve individual problems, but scholars
build tools to solve communities’ problems.  PocketHinman, a pocket
version of a [Hinman
Collator](https://www.youtube.com/watch?v=AHzn5y4KcJk), aims to help
individual scholars understand how seemingly identical texts, images,
and objects vary.  It’s a tool that they can use at home, in a
library, or anywhere they want to study.  Because the idea of [blink
comparison](https://en.wikipedia.org/wiki/Blink_comparator) undergirds
the design, the app has many more applications than the original aims.

The initial design was created by J. P. Ascher and DeVan Ard, with
advice from Elizabeth Ott, Devin Fitzgerald, and David Levy, and with
funding from the [Institute of Humanities and Global
Cultures](https://ihgc.as.virginia.edu) at the University of Virginia.
[Ross Harding](https://www.rossharding.me/#/pockethinman/) coded the
first version.  Sam Lemley and Neal Curtis refined it during a second
round of funding.  [Vaarnan Drolia](https://vaarnan.com) updated the
app for iOS 17 and squashed a few bugs with funding from the
[Bibliographical Society](https://bibsoc.org.uk).  J. P. Ascher
initially developed the idea during David Vander Meulen’s “Books as
Physical Objects” in a final paper.

J. P. keeps the app available and has written more extensively on
the design philosophy in the final partition of his [dissertation,
“Reading for Enlightenment in the Beginning of *Philosophical
Transactions*.”](https://search.lib.virginia.edu/sources/uva_library/items/02870w67p)

We give the code freely to anyone who wants to modify or improve on
it: [for iOS](https://github.com/vellvisher/PocketHinman-iOS) or [for Android](https://github.com/rnh8wb/PocketHinman-Android).
