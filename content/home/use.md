---
title: "How to Use"
weight: 2
---

When run, the app shows a live camera view with a toolbar across the
top and another across the bottom.  The toolbar on the top controls
the flicker, the bottom the capture.  To use the app:

First, take a photo or select a photo from your library.

Second, line up the semi-transparent image with what the camera shows.

Third, press the play (▷) button in the upper right corner to flicker.

### Top toolbar

![Top toolbar](speed%20slider.png)

Move the slider to the left to slow the blink, to the right to go
faster.  Hit the play (▷) button to start blinking, the pause ( ‖ ) button
to stop.

### Bottom toolbar

![Bottom toolbar](buttons.png)

The bottom toolbar lets you:

Select an existing image from your photo library,

Adjust the photo that you’re comparing with the live video: [alpha measures transparency](https://en.wikipedia.org/wiki/Alpha_transparency) of the overlay and panning and resizing helps adjust the photo for comparison,

Take a new photo to compare with the live video, or

Clear the photo you’ve selected so you can pick a new one.
